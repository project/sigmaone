<?php
/**
 * @file
 * Special Block plugin for logo
 */

/**
 * Function to return renderable arrays
 */
function logo_block_renderable_array($options, &$variables) {
  global $vtcore;

  // Declare status and record it in variables so other plugin can use it.
  $variables['show_site_name'] = (bool) vtcore_get_plugin_setting('toggle_logo', 'layout', 1);
  $variables['show_site_name'] = (bool) vtcore_get_plugin_setting('toggle_name', 'layout', 1);
  $variables['show_site_slogan'] = (bool) vtcore_get_plugin_setting('toggle_slogan', 'layout', 1);

  // Building Logo
  $logo_image = $variables['logo'];

  // Note: Logo will be turned off automatically without need for extra variables checking
  if (!empty($logo_image)) {
    $attributes = array(
      'id' => 'logo',
      'rel' => 'home',
      'title' => t('Home'),
    );

    $logo_image = theme('image', array('path' => $variables['logo'], 'alt' => t('Home')));
    // Drupal seems won't build the right url for frontpage using l(),
    // Thus we rebuild this url manually.
    $logo = '<a href="' . $variables['front_page'] . '"' . drupal_attributes($attributes) . '>' . $logo_image . '</a>';
  }


  // Building Slogan
  $slogan = $variables['site_slogan'];

  if (empty($slogan) && $variables['show_site_slogan'] == TRUE) {
    $slogan = filter_xss_admin(variable_get('site_slogan', ''));
  }

  // Overriding drupal standard if user configured the slogan
  // via .layout file or via region manager plugin
  // Note: " " will considered NOT empty
  $custom = vtcore_get_default_value('#site_slogan_enable', $options, 1);
  if (!empty($options['#site_slogan']) && $custom == ENABLED) {
    $slogan = $options['#site_slogan'];
  }

  if (!empty($slogan)) {
    $attributes = array(
      'id' => 'site-slogan',
      // Backward compat
      'class' => array('slogan'),
    );

    if ($variables['show_site_slogan'] == FALSE) {
      $attributes['class'] = 'element-invisible';
    }

    $slogan = '<div ' . drupal_attributes($attributes) . '>' . $slogan . '</div>';
  }

  // Building Titles
  $title = $variables['site_name'];

  if (empty($title) && $variables['show_site_name'] == TRUE) {
    $title = filter_xss_admin(variable_get('site_name', ''));
  }

  // Overriding drupal standard if user configured the title
  // via .layout file or via region manager plugin
  // Note: " " will considered NOT empty
  $custom = vtcore_get_default_value('#site_title_enable', $options, 1);
  if (!empty($options['#site_title']) && $custom == ENABLED) {
    $title = $options['#site_title'];
  }

  // Only build title if we got valid title
  if (!empty($title)) {
    $attributes = array(
      'id' => 'site-name',
      // Backward compat
      'class' => array('site-title'),
    );

    if ($variables['show_site_name'] == FALSE) {
      $attributes['class'] = 'element-invisible';
    }

    // Don't build using l() it will induce bug when Drupal
    // is installed in a sub-folder.
    $title_url = '<a href="' . $variables['front_page'] . '" title="' . t('Home') . '" rel="home" class="site-title">' . $title . '</a>';
    $title = '<h1 ' . drupal_attributes($attributes) . '>' . $title_url . '</h1>';

    // Change wrapper element if we have a title element for SEO purposes
    if (!empty($variables['title'])) {
      $title = '<div ' . drupal_attributes($attributes) . '>' . $title_url . '</div>';
    }
  }

  // Build custom class for main block wrapper
  $attributes = array(
    'class' => array(
      'branding',
      _layout_column_class($options['#column']),
    ),
  );

  if ($options['#clearfix'] == ENABLED) {
    $attributes['class'][] = 'clear clearfix';
  }

  if ($options['#newrow'] == ENABLED) {
    $attributes['class'][] = 'newrow';
  }

  if ($options['#lastrow'] == ENABLED) {
    $attributes['class'][] = 'lastrow';
  }

  $blocks = array(
    '#block_type' => 'logo',
    '#markup' => '<div ' . drupal_attributes($attributes) . '>' . $logo . $title . $slogan . '</div>',
    '#weight' => $options['#weight'],
    '#attached' => array(
  		'css' => array($vtcore->plugin_path . '/layout/blocks/logo/css/logo.css'),
    ),
  );

  return $blocks;
}