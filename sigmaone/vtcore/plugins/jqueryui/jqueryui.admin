<?php

/**
 * Implement hook_vtcore_settings()
 */
function jqueryui_vtcore_settings_alter_process(&$form, &$form_state) {
  global $vtcore;

  // Don't rely on global $theme_key
  // Use one in form state instead
  $theme_key = $form_state['build_info']['args'][0];
  $theme_path = $form_state['build_info']['theme']['theme_path'];
  $base_theme_path = $form_state['build_info']['theme']['base_theme_path'];

  // User area specific settings
  $form['jqueryui'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery UI Manager'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => -19,
    '#group' => 'theme_core',
    '#tree' => FALSE,
    '#attached' => array(
      'js' => array(
        $base_theme_path . '/vtcore/plugins/jqueryui/js/admin/jqueryui.admin.ajax.js',
      ),
    ),
  );

  // Get plugins stored themes
  $dir = $base_theme_path . '/vtcore/plugins/jqueryui/themes';
  $options = array('key' => 'uri');
  $files = file_scan_directory($dir, '/.*\.css$/', $options);

  // Get per theme stored theme
  $theme_dir = $theme_path . '/jqueryui/themes';
  $theme_files = file_scan_directory($theme_dir, '/.*\.css$/', $options);

  // Merge both per theme and plugin stored themes
  $files = array_merge($files, $theme_files);

  $select_options = array();
  $theme_data = array();
  if (!empty($files) && is_array($files)) {
    foreach ($files as $url => $file) {
      if ($file->name == 'jqueryui-demo') {
        continue;
      }
      $select_options[$file->name] = ucfirst($file->name);
      $theme_data[$file->name] = $url;
    }
  }

  $selected_theme = vtcore_get_plugin_setting('jquery_theme_selected', 'jqueryui', 'redmond');

  if (!empty($select_options)) {
    $theme_key = $form_state['build_info']['args'][0];
    $form['jqueryui']['jquery_theme_selected'] = array(
      '#type' => 'select',
      '#title' => t('Select jQuery theme to use'),
      '#options' => $select_options,
      '#description' => t('You can download more theme from jQuery UI theme roller and place them in the plugin folder.
      										 <br/>The final directory structure should be : #path.
      										 <br/>The css file name should not matter as long as it is the only css file inside the direactory and have .css as the file extension.'
                           , array('#path' => $dir . '/jQuery_ui_theme_name/jQuery_ui_theme.css')),
      '#default_value' => $selected_theme,
      '#ajax' => array(
        'callback' => 'jqueryui_ajax_refresh',
        'wrapper' => 'jqueryui-demo-sample',
        'method' => 'replace',
        'effect' => 'slide',
        'path' => 'system/ajax/theme-' . $theme_key,
        'ajax_page_state' => BASE_THEME,
      ),
    );
  }
  else {
    $form['jquery_ui']['empty_theme'] = array(
      '#type' => 'markup',
      '#markup' => t('No jQuery theme found, please upload one to plugin theme folder.
      								<br/>The final directory structure should be : #path
    									<br/>The css file name should not matter as long as it is the only css file inside the direactory and have .css as the file extension.'
                      , array('#path' => $dir . '/jQuery_ui_theme_name/jQuery_ui_theme.css')),
    );
  }

  if (!empty($form_state['values']['jquery_theme_selected'])) {
    $selected_theme = $form_state['values']['jquery_theme_selected'];
  }

  $paths = pathinfo($theme_data[$selected_theme]);
  $filename = $paths['dirname'] . '/jqueryui-demo.css';

  // Only build the demo custom css once
  // upon updating the original css you must
  // delete the jqueryui-demo.css file found
  // in the directory to force this form to rebuild
  // the demo css.
  if (!file_exists($filename)) {
    // Build custom css
    $file = fopen($theme_data[$selected_theme], 'r');
    $css = fread($file, filesize($theme_data[$selected_theme]));
    $css = jqueryui_add_css_prefix('#jqueryui-demo-sample.' . $selected_theme, $css);

    file_unmanaged_save_data($css, $filename, FILE_EXISTS_REPLACE);

    // Save memory usage
    fclose($file);
    unset($css);
  }

  $form['jqueryui']['form_sample'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('jQuery UI Theme Preview'),
    '#description' => t('Preview selected jQueryUI theme results'),
    '#collapsible' => FALSE,
    '#attributes' => array(
    	'id' => 'jqueryui-demo-sample',
    	'class' => array($selected_theme),
    ),
    '#attached' => array(
      'css' => array($filename),
    ),
  );

  $form['jqueryui']['form_sample']['textfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Textfield sample'),
  );

  $form['jqueryui']['form_sample']['textarea'] = array(
    '#type' => 'textarea',
    '#title' => t('Resizeable textarea sample'),
    '#resizeable' => TRUE,
  );

  $form['jqueryui']['form_sample']['select'] = array(
    '#type' => 'select',
    '#title' => t('Select sample'),
    '#options' => array('sample one', 'sample two', 'sample three'),
  );

  $form['jqueryui']['form_sample']['selects'] = array(
    '#type' => 'select',
    '#title' => t('Multiple select sample'),
    '#options' => array('sample one', 'sample two', 'sample three'),
    '#multiple' => TRUE,
  );

  $form['jqueryui']['form_sample']['checkbox'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Checkbox sample'),
  );

  $form['jqueryui']['form_sample']['checkboxes'] = array(
    '#type'  => 'checkboxes',
    '#title' => t('Checkboxes sample'),
    '#default_value' => array(0),
    '#options' => array(
      'one',
      'two',
      'three',
    ),
  );

  $form['jqueryui']['form_sample']['radio'] = array(
    '#type' => 'radio',
    '#title' => t('Radio sample'),
  );

  $form['jqueryui']['form_sample']['radios'] = array(
    '#type' => 'radios',
    '#title' => t('Radios sample'),
  	'#default_value' => array(0),
    '#options' => array(
      'one',
      'two',
      'three',
    ),
  );

  $form['jqueryui']['form_sample']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password sample'),
  );

  $form['jqueryui']['form_sample']['date'] = array(
    '#type' => 'date',
    '#title' => t('Date sample'),
  );

  $form['jqueryui']['form_sample']['file'] = array(
    '#type' => 'file',
    '#title' => t('File sample'),
  );

  $form['jqueryui']['form_sample']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('weight sample'),
  );

  $form['jqueryui']['form_sample']['managed_file'] = array(
    '#type' => 'file',
    '#title' => t('Managed File sample'),
  );

  $form['jqueryui']['form_sample']['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Collapsible fieldset sample'),
    '#description' => t('Sampe Description'),
    '#collapsible' => TRUE,
  );

  $form['jqueryui']['form_sample']['button'] = array(
    '#type' => 'button',
    '#value' => t('Sample Button'),
  );

  $header = array(
    'header one',
    'header two',
    'header three',
    'header four',
  );

  $options = array(
    array('sample one', 'sample two', 'sample three', 'sample four'),
    array('sample one', 'sample two', 'sample three', 'sample four'),
    array('sample one', 'sample two', 'sample three', 'sample four'),
  );

  $form['jqueryui']['form_sample']['table_select'] = array(
    '#type' => 'table_select',
    '#title' => t('Table select sample'),
    '#header' => $header,
    '#options' => $options,
  );

  $form['jqueryui']['form_sample']['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#title' => t('Vertical tabs sample'),
  );

  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_one'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
  );

  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_two'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
    '#collapsible' => true,
  );

  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_two']['fieldset_sample_three'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
  	'#collapsible' => true,
  );
  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_two']['fieldset_sample_three']['fieldset_sample_two'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
  	'#collapsible' => true,
  );
  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_two']['fieldset_sample_three']['fieldset_sample_two']['fieldset_sample_one'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
    '#collapsible' => true,
  );


  $form['jqueryui']['form_sample']['vertical_tabs']['fieldset_sample_three'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vertical tabs sample'),
    '#description' => t('Sample description'),
  );

  if (!isset($form['#submit']) || !in_array('jqueryui_settings_submit', $form['#submit'])) {
    $form['#submit'][] = 'jqueryui_settings_submit';
  }

  // Additional plugin form that need to be altered
  if (!isset($form['#submit']) ||  isset($form['region'])) {
    _jqueryui_attach_css(BASE_THEME, 'regionarea', $form);
    _jqueryui_attach_js(BASE_THEME, 'regionarea', $form);
  }
}

/**
 * Ajax callback function
 */
function jqueryui_ajax_refresh($form, &$form_state) {
  return $form['jqueryui']['form_sample'];
}

/**
 * Extra submit function for jQueryUI plugin
 */
function jqueryui_settings_submit(&$form, &$form_state) {
  // Don't record preview forms value
  unset($form_state['values']['form_sample']);
  unset($form_state['storage']['old_value']['form_sample']);
}

/**
 * Function to add prefix to each css selector
 */
function jqueryui_add_css_prefix($prefix, $css) {
  $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
  $parts = explode('}', $css);
  foreach ($parts as &$part) {
    if (empty($part)) {
      continue;
    }
    $mainparts = explode('{', $part);

    $selectors = array_shift($mainparts);

    $subParts = explode(',', $selectors);

    foreach ($subParts as &$subPart) {
      $subPart = $prefix . ' ' . trim($subPart);
    }

    $selectors = implode(', ', $subParts);

    array_unshift($mainparts, $selectors);

    $part = implode(' {', $mainparts);
  }

  return implode("}\n", $parts);
}