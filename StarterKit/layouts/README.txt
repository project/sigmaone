All .layout file specific to this theme must be stored here.

The subfolder :
1. theme_custom
2. theme_default
3. theme_dynamic

is actually hard coded to VTCore, so don't change the name.

Folder : theme_custom
If you create .layout file manually and wants to override 
theme default layout, please store it here.

Folder : theme_default
This is for the theme default layout files, it will get overriden
by files stored in theme_custom

Folder : theme_dynamic
This file is for Region area plugin storage, the file stored here
will be overriden if you configure the same layout from regionarea
plugin GUI.