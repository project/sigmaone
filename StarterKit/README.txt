StarterKit for SigmaOne Theme

This is a template for creating a subtheme for SigmaOne

Please edit the StaterKit.info and change the staterkit folder name and the starterkit.info filename
to match your new subtheme.

In this staterkit provides :

1. CSS file sample to override SigmaOne, you'll need to edit the .info file
to enable the CSS overriding.

2. jQueryUI Folder for storing jQueryUI theme for use with this theme.

3. Js file to be used with this subtheme

4. Layouts file structure and default .layout file for use with this subtheme.

5. Color module integration sample files.

