jQueryUI Themes

You can download new themes from theme roller at http://www.jqueryui.com.
But since the downloaded theme is not 100% compatible with Drupal, please
see the default theme provided by sigmaone at sigmaone/vtcore/plugins/jqueryui/themes/redmond
and see what new CSS entry required to make the new theme 100% Drupal 
compatible.

The final structure after downloading the theme from jqueryui.com is :

StarterKit(or your new theme name)/jqueryui/themes/new_theme_name(eg. redmond)/new_theme_name.css(eg. redmond.css)

Don't forget to select the new theme when you visit the theme configuration page.s